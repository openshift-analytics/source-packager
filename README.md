# Source Extractor

This analyser will read OpenShift buildlogs and figure out what source code went into the final container image.

# Deployment

This project depends on OpenShift, EnMasse Barnabas and Heptio Eventrouter. 

## Prerequisites

To set up a local integration environment use:

```
oc cluster up 
oc new-project analytics --description='This is OpenShift Analytics, see https://gitlab.com/openshift-analytics/'
oc create -f https://gitlab.com/openshift-analytics/source-packager/raw/master/openshift-template.yaml
oc create -f https://gitlab.com/openshift-analytics/raw-events-switchyard/raw/master/openshift-template.yaml
oc create -f https://gitlab.com/goern/eventrouter/raw/master/openshift-template.yaml

oc login -u system:admin # login as admin
oc adm policy add-cluster-role-to-user edit system:serviceaccount:analytics:default # grant priv to see all pods in all namespaces
oc adm policy add-cluster-role-to-user view developer # grant priv to see all pods in all namespaces

oc login -u developer # drop privileges
oc import-image registry.access.redhat.com/rhel7-atomic --confirm

oc process -f https://raw.githubusercontent.com/EnMasseProject/barnabas/master/kafka-statefulsets/resources/openshift-template.yaml --param IMAGE_REPO_NAME=goern --labels app=apache-kafka| oc create -f -
```

After some time you should see Zookeeper (1 pod) and Kafka (3 pods) deployed in project analytics.

## Configuration

Some of the components need runtime configuration, so lets create a ConfigMap for that...

```
oc create -f - <<EOF
  kind: ConfigMap
  apiVersion: v1
  metadata:
    name: event-stream-analytics
  data:
    KAFKA_PEERS: kafka:9092
    SERVICE_PORT: "8080"
    RAW_TOPIC_NAME: openshift.raw
    PROJECTS_TOPIC_NAME: openshift.projects
    IMAGESTREAM_TOPIC_NAME: openshift.imagestreams
    DEPLOYMENTS_TOPIC_NAME: openshift.deployments
    BUILDS_DISCOVERED_TOPIC_NAME: openshift.builds.discovered
    SOURCE_EXTRACTOR_DELETE_BUILDCONFIG_AFTER_BUILD: "False"  
EOF
```

## Deploy

```
oc new-app eventrouter
oc new-app event-stream-analytics-raw-events-switchyard
oc new-app event-stream-analytics-event-stream-analytics
oc new-app source-packager
```

## Testing

Create a new project testing-1 and deploy a Django application, watch the Builds and ImageStreams closely.

# Development

To do some development, you will need Apache Kafka and OpenShift up and running.

## Prerequisites

A Kafka server... see https://kafka.apache.org/quickstart how to set that up. Some test data is at `tests/fixtures/build-events.json` thay can be published to Kafka using `tests/feed_to_openshift.builds.discovered.py`

## Deploy

Deploy it via `openshift-tempalte.yaml` and add a binary python build to build from local source.

# Conclusion

UNFINISHED

## Advantages...

Decoupling source extraction from the build process will schedule workloads more efficiently and not slow down the Build.

## Drawbacks... 

It might that the git repository and commit hash disapear after the OpenShift Build finished and before source-extractors Build starts.

It might be that we dont see all commands which pull in source code, s2i assemble script may obfuscate some commands.

The source-extractor Build might run on a different set of enabled repositories or a different pip/npm/mvn mirror. Reconstructing src via `pip` will always involve a build, which is either intended nor possible (with the current s2c-python image).