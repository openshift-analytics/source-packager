#!/usr/bin/env python

"""SourceExtractor.py: SourceExtractor will observe finished OpenShift Builds and package the source used in this build into a new container image.
"""

from __future__ import with_statement, print_function

__version__ = '0.1.0'

from flask import Flask
from prometheus_client import generate_latest, REGISTRY, Counter, Histogram, Summary

from kafka import KafkaConsumer
from kafka.errors import KafkaError

from jinja2 import Template

import requests
import json
import logging
import os
import threading
import urllib3
import time

# Configuration
# Application / General
DEBUG_LOG_LEVEL = bool(os.getenv('DEBUG', False))
DELETE_BUILDCONFIG_AFTER_BUILD = bool(
    os.getenv('DELETE_BUILDCONFIG_AFTER_BUILD', False))

# OpenShift
OPENSHIFT_API_ENDPOINT = 'https://127.0.0.1:8443/oapi/v1/'
BEARER_TOKEN = os.getenv('BEARER_TOKEN', 'UNSET')

# Kafka
BOOTSTRAP_SERVERS = os.getenv('KAFKA_PEERS', 'localhost:9092')
BUILDS_COMPLETED_TOPIC_NAME = os.getenv(
    'BUILDS_COMPLETED_TOPIC_NAME', 'openshift.builds.discovered')
BUILDS_COMPLETED_TOPIC_NAME = 'openshift.builds.completed'

# Templates for API calls
EXTRACTOR_CREATED_IMAGESTREAM = Template("""
kind: ImageStream
apiVersion: v1
metadata:
    name: '{{ name }}-src'
    namespace: '{{ namespace }}'
    labels:
        bot: source-packager
spec:
    lookupPolicy:
        local: false
status:
    dockerImageRepository: ''
""")
EXTRACTOR_CREATED_BUILD_CONFIG = Template("""
kind: BuildConfig
apiVersion: v1
metadata:
    name: source-packager-{{ build }}-{{ git_commit_hash }}
    namespace: {{ namespace }}
    labels:
        app: {{ build }}
        bot: source-packager
spec:
    strategy:
        type: Docker
        dockerStrategy: null
        from:
            kind: ImageStreamTag
            name: rhel7-atomic:latest
    source:
        type: Dockerfile
        dockerfile: |-
            FROM centos/python-35-centos7
            
            MAINTAINER <goern@redhat.com>
            
            LABEL Author="source-packager - A bot" \
                  summary="Source-to-Container - source code packager for {{ build }}" \
                  description="This is a source container created by source-packager, Git: {{ git_repo }} ({{ git_commit_hash }})" \
                  io.k8s.description="This is a source container created by source-packager, Git: {{ git_repo }} ({{ git_commit_hash }})" \
                  io.openshift.tags="openshift-analytics,source-packager,source-container,s2c,{{ git_commit_hash }}" \
                  vendor="fill in some name" \
                  license="fill in some name" \
                  version="{{ git_commit_hash }}" \
                  name="source-packager-{{ build }}-{{ git_commit_hash }}"
                  
            RUN source scl_source enable rh-python35 && \
                virtualenv /opt/app-root && \
                mkdir /opt/app-root/app-src && cd /opt/app-root/app-src && git clone {{ git_repo }} && cd `basename {{ git_repo }} .git` && git checkout {{ git_commit_hash }} && \
                pip install --user --build /tmp/ --requirement requirements.txt
                  
            CMD /bin/echo "This is just a Source Container, not a binary!"
    output:
        to:
            kind: ImageStreamTag
            namespace: {{ namespace }}
            name: {{ build }}-src:{{ git_commit_hash }}
""")
EXTRACTOR_BUILD_TRIGGER = Template("""
kind: BuildRequest
apiVersion: v1
metadata:
    name: {{ name }}
    creationTimestamp: null
triggeredBy:
    - message: source-packager - A bot
""")


# Application
class ImageStreamConsumer:

    def __init__(self, endpoint, token, bootstrap_servers=['localhost:9092']):
        self.threads = []
        self.endpoint = endpoint
        self.token = token

        self.bootstrap_servers = bootstrap_servers

        self.consumer = KafkaConsumer(BUILDS_COMPLETED_TOPIC_NAME,
                                      value_deserializer=lambda m: json.loads(
                                          m.decode('utf-8')),
                                      bootstrap_servers=self.bootstrap_servers)

    def consume(self):
        for message in self.consumer:
            logger.debug("%s:%d:%d: key=%s value=%s" % (message.topic, message.partition,
                                                        message.offset, message.key,
                                                        message.value))

            build_event = message.value  # this is a json object

            # dont recurse on stuff I created!
            if build_event['metadata']['name'].startswith('source-packager'):
                continue

            logger.info("build '%s/%s' has finished!" %
                        (build_event['metadata']['namespace'], build_event['metadata']['name']))  # TODO maybe we want to strip -n from the build name?!

            t = threading.Thread(target=self._fetch_build_log, args=(
                build_event['metadata']['namespace'], build_event['metadata']['name'],))
            self.threads.append(t)
            t.start()

    def _create_imagestream(self, namespace, name):
        logger.info("creating ImageStream '%s/%s-src'" %
                    (namespace, name))

        self._create_openshift_resource(namespace, 'imagestreams', EXTRACTOR_CREATED_IMAGESTREAM.render(
            name=name, namespace=namespace))

    def _create_build_config(self, namespace, name, git_repo, git_commit_hash):
        logger.info("creating BuildConfig '%s/source-packager-%s-%s' source from: %s (%s)" %
                    (namespace, name, git_commit_hash, git_repo, git_commit_hash))

        self._create_openshift_resource(namespace, 'buildconfigs', EXTRACTOR_CREATED_BUILD_CONFIG.render(build=name,
                                                                                                         namespace=namespace, git_repo=git_repo,
                                                                                                         git_commit_hash=git_commit_hash))
        self._instantiate_buildconfig(
            namespace, "source-packager-%s-%s" % (name, git_commit_hash))

    def _instantiate_buildconfig(self, namespace, name):
        logger.info("instantiate BuildConfig '%s/%s'" % (namespace, name))

        headers = {
            "Authorization": "Bearer %s" % self.token,
            "Content-Type": "application/yaml",
            "Accept": "application/yaml",
        }

        try:
            res = requests.post(self.endpoint + "namespaces/%s/buildconfigs/%s/instantiate" % (namespace, name),
                                headers=headers, verify=False, data=EXTRACTOR_BUILD_TRIGGER.render(name=name))

            if res.status_code == 401:  # 4xx CLIENT ERROR 401 UNAUTHORIZED
                logger.warn("user is not authorized to edit namespace %s" %
                            (namespace))
                return res.status_code
            if res.status_code == 201:  # 2xx SUCCESS 201 CREATED
                pass
            else:
                logger.error(res.status_code)
                return res.status_code

            # TODO if we shall delete the generated BC lets do to!
            if DELETE_BUILDCONFIG_AFTER_BUILD:
                logger.info("deleting BuildConfig '%s/%s'" % (namespace, name))

                # check if build as finished, if so: delete BC
                res = requests.delete(self.endpoint + "namespaces/%s/buildconfigs/%s" % (namespace, name),
                                      headers=headers, verify=False, data=EXTRACTOR_BUILD_TRIGGER.render(name=name))

                if res.status_code != 200:
                    logger.error(res.status_code)
                    return res.status_code

        # TODO except more specific
        except Exception as e:
            logger.debug(self.endpoint + "namespaces/%s/buildconfigs/%s/instantiate" % (namespace, name),
                         headers=headers, verify=False)
            logger.error(e)
            return

    def _create_openshift_resource(self, namespace, resource_type, resource_definition):
        headers = {
            "Authorization": "Bearer %s" % self.token,
            "Content-Type": "application/yaml",
            "Accept": "application/yaml",
        }

        try:
            res = requests.post(self.endpoint + "namespaces/%s/%s/" % (namespace, resource_type),
                                headers=headers, verify=False, data=resource_definition)

            # TODO what should we do if this BuildConfig or ImageStream already
            # exists?
            if res.status_code == 409:
                logger.warn("resource creation conflict: %snamespaces/%s/%s" %
                            (self.endpoint, namespace, resource_type))
                return res.status_code
            elif res.status_code == 401:
                logger.warn("user is not authorized to edit namespace %s" %
                            (namespace))
                return res.status_code
            elif res.status_code == 201:
                logger.info("resource created: %snamespaces/%s/%s" %
                            (self.endpoint, namespace, resource_type))
            else:
                logger.error(res.status_code)
                return res.status_code

        # TODO except more specific
        except Exception as e:
            logger.debug(self.endpoint + "namespaces/%s/%s" % (namespace, resource_type),
                         headers=headers, verify=False)
            logger.error(e)
            return

    def _analyse_buildlog(self, build_log_lines):
        git_repo = 'NOTFOUND'
        git_commit_hash = 'NOTFOUND'
        python_modules_by_pip = []

        for line in build_log_lines:
            if line.strip().startswith("Cloning"):  # ah we are cloning
                git_repo = line.split(" ")[1].strip('"')
                next_is_my_commit = True
            elif line.strip().startswith("Commit") and next_is_my_commit:  # so this must be the git commit hash
                git_commit_hash = line.split(":")[1].split(" ")[0].strip()
                next_is_my_commit = False
            elif line.startswith("Downloading "):  # these are the python modules
                python_modules_by_pip.append(line.split(" ")[1])

        return git_repo, git_commit_hash, python_modules_by_pip

    def _fetch_build_log(self, namespace, name):
        next_is_my_commit = False

        start = time.time()

        logger.info("fetching buildlog of '%s/%s', will analyse it..." %
                    (namespace, name))

        # TODO if we timeout on this request, do not ack the message
        headers = {
            "Authorization": "Bearer %s" % self.token,
        }

        try:
            build_log = requests.get(self.endpoint + "namespaces/%s/builds/%s/log" %
                                     (namespace, name), headers=headers, verify=False)

            if build_log.status_code == 401:
                logger.warn("user is not authorized to view namespaces/%s/builds/%s/log" %
                            (namespace, name))
                return

            blc = build_log.content
            build_log_lines = blc.decode('utf-8').splitlines()

            git_repo, git_commit_hash, python_modules = self._analyse_buildlog(
                build_log_lines)

            # so how do we build a container image with all that source?
            if git_repo != 'NOTFOUND' and git_commit_hash != 'NOTFOUND':
                logger.info("build '%s/%s' was based on %s (%s)" %
                            (namespace, name, git_repo, git_commit_hash))

                self._create_imagestream(namespace, name)
                self._create_build_config(
                    namespace, name, git_repo, git_commit_hash)
            else:
                logger.error(
                    "git repository and/or commit hash could not be found in buildlog: %s, %s" % (git_repo, git_commit_hash))

            end = time.time()

            logger.info("buildlog of '%s/%s' has been downloaded and analysed in %d seconds" %
                        (namespace, name, end - start))

        # TODO except more specific
        except Exception as e:
            logger.debug(self.endpoint + "namespaces/%s/builds/%s/log" % (namespace, name),
                         headers=headers, verify=False)
            logger.error(e)
            return


if __name__ == "__main__":
    if DEBUG_LOG_LEVEL:
        logging.basicConfig(level=logging.DEBUG)
    else:
        logging.basicConfig(level=logging.INFO)
        urllib3.disable_warnings()

    logger = logging.getLogger(__name__)

    logger.info("%s %s", __name__, __version__)
    logger.info('initializing...')
    logger.info("consuming event from %s" %
                (BUILDS_COMPLETED_TOPIC_NAME))

    if DELETE_BUILDCONFIG_AFTER_BUILD:
        logger.info(
            'deleting generated BuildConfig after Build has finished...')

    # if we can read bearer token from /var/run/secrets/kubernetes.io/serviceaccount/token use it,
    # otherwise use the one from env
    try:
        logger.debug(
            'trying to get bearer token from secrets file within pod...')
        with open('/var/run/secrets/kubernetes.io/serviceaccount/token') as f:
            BEARER_TOKEN = f.read()

        # if we can read bearer token from file, use openshift.svc as anedpoint
        # otherwise use localhost
        OPENSHIFT_API_ENDPOINT = 'https://openshift.default.svc.cluster.local/oapi/v1/'

    except:
        logger.info("not running within an OpenShift cluster...")

    logger.debug("Using %s and Bearer token %s" %
                 (OPENSHIFT_API_ENDPOINT, BEARER_TOKEN))

    isc = ImageStreamConsumer(OPENSHIFT_API_ENDPOINT, BEARER_TOKEN,
                              BOOTSTRAP_SERVERS)
    isc.consume()
